/* eslint-disable react-hooks/exhaustive-deps */
import { useState, useLayoutEffect, useEffect } from 'react';
import styles from './Lottery.module.scss';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import Notify from '../../components/Notify';

const Lottery = () => {
    const router = useRouter();

    const [openLoading, setOpenLoading] = useState(false);
    const [openSnack, setOpenSnack] = useState(false);
    const [alertType, setAlertType] = useState('success');
    const [snackMsg, setSnackMsg] = useState('');

    const [currentUserTicket, setCurentUserTicket] = useState(100);
    const [choosenNumber, setChoosenNumber] = useState('');

    const onCloseSnack = () => {
        setOpenSnack(false);
    };

    const onShowResult = ({ type, msg }) => {
        setOpenSnack(true);
        setOpenLoading(false);
        setAlertType(type);
        setSnackMsg(msg);
    };

    const onBuyTicket = (number) => {
        if (number === '' || number < 0 || number >= 100) {
            onShowResult({
                type: 'error',
                msg: 'Ticket number must be >= 0 and < 100.',
            });
        } else {
            setCurentUserTicket(number);
        }
    };

    const onSuccess = () => {
        onShowResult({
            type: 'success',
            msg: 'copied',
        });
    };

    return (
        <>
            <Notify openLoading={openLoading} openSnack={openSnack} alertType={alertType} snackMsg={snackMsg} onClose={onCloseSnack} />
            <div className={styles.root}>
                <div className={styles.label_create}>Lottery Game: Find out the luckiest players!</div>
                <br />
                <div className={styles.text_description}>
                    <div>Your will deposite 1 NEAR for each number, each user can buy only one number in each lottery game.</div>
                    <div>10% of the total deposit will be returned to the dealer.</div>
                    <div>If there are more than 1 winner, the coin/token will be shared.</div>
                </div>
                <div>
                    {currentUserTicket !== 100 ? (
                        <div>
                            <div className={styles.label}>
                                <div className={styles.label_title}>Your number in the current game</div>
                            </div>
                            <div className={styles.line} />
                            <div className={styles.alert_message}>Your bought number is:</div>
                            <div className={styles.ticket_number}>{currentUserTicket}</div>
                        </div>
                    ) : (
                        <div>
                            <div className={styles.label}>
                                <div className={styles.label_title}>Get your number in the next game</div>
                            </div>
                            <div className={styles.line} />
                            <br />
                            <div className={styles.search_row}>
                                <div className={styles.search_area}>
                                    <input
                                        placeholder={'Type your number which you choose, value must be in range 00 to 99'}
                                        className={styles.choosen_number}
                                        value={choosenNumber}
                                        onChange={(e) => {
                                            setChoosenNumber(e.currentTarget.value);
                                        }}
                                    />
                                </div>
                                <button className={styles.button_search} onClick={() => onBuyTicket(choosenNumber)}>
                                    Buy
                                </button>
                            </div>
                        </div>
                    )}
                </div>
            </div>
        </>
    );
};

export default Lottery;
