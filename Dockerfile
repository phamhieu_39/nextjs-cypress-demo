FROM node:16

RUN apt-get update && apt-get install -y libaio1 wget unzip

WORKDIR /app

COPY ["package.json", "./"]

RUN npm install

COPY . .

RUN npm run build

CMD [ "npm", "run", "start" ]