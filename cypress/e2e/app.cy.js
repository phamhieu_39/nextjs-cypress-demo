/* eslint-disable */
// Disable ESLint to prevent failing linting inside the Next.js repo.
// If you're using ESLint on your project, we recommend installing the ESLint Cypress plugin instead:
// https://github.com/cypress-io/eslint-plugin-cypress

describe('Navigation', () => {
  it('should navigate to the Lottery page', () => {
    // Start from the index page
    cy.visit('http://localhost:3000/')

    // Find a link with an href attribute containing "lottery" and click it
    cy.get('div').contains('Lottery Game').click()

    // The new url should include "/lottery"
    cy.url().should('include', '/lottery')

    // The new page should contain an h1 with "About page"
    cy.get('div').contains('Lottery Game: Find out the luckiest players!')
  })
  it('should navigate to the How to play page', () => {
    // Start from the index page
    cy.visit('http://localhost:3000/')

    cy.get('div').contains('How to play').click()

    cy.url().should('include', 'how_to_play')

    cy.get('div').contains('How to play?')
    cy.get('div').contains('How the Lottery Game work?')
    cy.get('div').contains('How the Hui Game work?')

  })
})

describe('BuyFeature', () => {
  it('Input invalid number', () => {
    // Start from the index page
    cy.visit('http://localhost:3000/lottery')
    cy.get('input').type('-1')
    cy.get('button').contains('Buy').click()
    cy.get('div').contains('Ticket number must be >= 0 and < 100.')
  })
  it('Input valid number', () => {
    // Start from the index page
    let expected_number = '10'
    cy.visit('http://localhost:3000/lottery')
    cy.get('input').type(expected_number)
    cy.get('button').contains('Buy').click()
    cy.get('div').contains('Your bought number is:')
    cy.get('div').contains(expected_number)
  })
})

// Prevent TypeScript from reading file as legacy script
export {}
